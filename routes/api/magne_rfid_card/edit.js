var router = require('express').Router();
var passport = require('passport');
var auth = require("../../auth")

router.patch('/', auth.required, function(req, res, next) {
  if (!req.body.id_card){
    return res.status(400).json({errors: {message: "id_card can't be blank"}});
  }
  return res.status(200).json({
  "success": true
  })
})

module.exports = router;