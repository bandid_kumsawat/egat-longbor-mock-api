var router = require('express').Router();
var passport = require('passport');
var auth = require("../../auth")

router.put('/', auth.required, function(req, res, next) {
  return res.status(200).json({
  "success": true
  })
})

module.exports = router;