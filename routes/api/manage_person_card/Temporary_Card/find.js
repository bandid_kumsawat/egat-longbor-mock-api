var router = require('express').Router();
var passport = require('passport');
var data1_10 = require("../../../../data/temporary_card/find1-10")
var data1_5 = require("../../../../data/temporary_card/find1-5")
var data6_10 = require("../../../../data/temporary_card/find6-10")
var auth = require("../../../auth")

router.post('/', auth.required, function(req, res, next) {
  // console.log(res)

  // if (!(req.payload.exp - req.payload.iat) > 0){
  //   return res.status(405).json({errors: {message: "Token is expired."}});
  // }
  // 400 - 499 handle client error.
  if (!req.body.offset){
    return res.status(400).json({errors: {message: "offset can't be blank"}});
  }
  if (!req.body.limit){
    return res.status(400).json({errors: {message: "limit can't be blank"}});
  }
  if (isNaN(req.body.offset)){
    return res.status(400).json({errors: {message: "offset can't be number"}});
  }
  if (isNaN(req.body.limit)){
    return res.status(400).json({errors: {message: "limit can't be number"}});
  }
  if (req.body.offset === "0" && req.body.limit === "10"){
    return res.status(200).json(data1_10)
  } else if (req.body.offset === "0" && req.body.limit === "5"){
    return res.status(200).json(data1_5)
  } else if (req.body.offset === "5" && req.body.limit === "5"){
    return res.status(200).json(data6_10)
  }

  return res.status(200).json({
    data: [],
    length: 0
  })
})

module.exports = router;