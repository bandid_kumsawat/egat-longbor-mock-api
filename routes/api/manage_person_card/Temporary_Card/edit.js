var router = require('express').Router();
var passport = require('passport');
var data1_10 = require("../../../../data/person_card/find1-10")
var data1_5 = require("../../../../data/person_card/find1-5")
var data6_10 = require("../../../../data/person_card/find6-10")
var auth = require("../../../auth")

router.patch('/', auth.required, function(req, res, next) {
  if (!req.body.id_person){
    return res.status(400).json({errors: {message: "id_person can't be blank"}});
  }
  return res.status(200).json({
  "success": true
  })
})

module.exports = router;