var router = require('express').Router();
var passport = require('passport');
var data1_10 = require("../../../../data/person_card/find1-10")
var data1_5 = require("../../../../data/person_card/find1-5")
var data6_10 = require("../../../../data/person_card/find6-10")
var auth = require("../../../auth")

router.put('/', auth.required, function(req, res, next) {
  return res.status(200).json({
  "success": true
  })
})

module.exports = router;