var router = require('express').Router();
var passport = require('passport');
var export_person = require("../../../../data/person_card/export_person")
var auth = require("../../../auth")

router.unlink('/card/return', auth.required, function(req, res, next) {
  if (!req.body.id_person){
    return res.status(400).json({errors: {message: "id_person can't be blank"}});
  }
  return res.status(200).json({
    "success": true
  })
})

module.exports = router;