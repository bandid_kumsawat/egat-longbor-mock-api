var router = require('express').Router();
var passport = require('passport');
var auth = require("../../auth")

router.link('/registor_card', auth.required, function(req, res, next) {
  if (!req.body.id_person){
    return res.status(400).json({errors: {message: "id_person can't be blank"}});
  }
  if (!req.body.qr_code){
    return res.status(400).json({errors: {message: "qr_code can't be blank"}});
  }
  if (!req.body.uhf_code){
    return res.status(400).json({errors: {message: "uhf_code can't be blank"}});
  }
  if (!req.body.mifare_code){
    return res.status(400).json({errors: {message: "mifare_code can't be blank"}});
  }

  return res.status(200).json({
    "success": true
  })
})

module.exports = router;